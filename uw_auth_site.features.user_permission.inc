<?php

/**
 * @file
 * uw_auth_site.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_auth_site_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer uw_auth_site settings'.
  $permissions['administer uw_auth_site settings'] = array(
    'name' => 'administer uw_auth_site settings',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_auth_site',
  );

  return $permissions;
}
