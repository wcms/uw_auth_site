<?php

/**
 * @file
 * uw_auth_site.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_auth_site_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-management_authenticated-site-settings:admin/config/system/auth_site.
  $menu_links['menu-site-management_authenticated-site-settings:admin/config/system/auth_site'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/auth_site',
    'router_path' => 'admin/config/system/auth_site',
    'link_title' => 'Authenticated site settings',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_authenticated-site-settings:admin/config/system/auth_site',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Authenticated site settings');

  return $menu_links;
}
