<?php

/**
 * @file
 * uw_auth_site.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_auth_site_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_user_default_permissions_alter().
 */
function uw_auth_site_user_default_permissions_alter(&$data) {
  if (isset($data['access comments'])) {
    $data['access comments']['roles']['WCMS support'] = 'WCMS support'; /* WAS: '' */
    $data['access comments']['roles']['award content author'] = 'award content author'; /* WAS: '' */
    $data['access comments']['roles']['content author'] = 'content author'; /* WAS: '' */
    $data['access comments']['roles']['content editor'] = 'content editor'; /* WAS: '' */
    $data['access comments']['roles']['emergency alerter'] = 'emergency alerter'; /* WAS: '' */
    $data['access comments']['roles']['events editor'] = 'events editor'; /* WAS: '' */
    $data['access comments']['roles']['form editor'] = 'form editor'; /* WAS: '' */
    $data['access comments']['roles']['notices editor'] = 'notices editor'; /* WAS: '' */
    $data['access comments']['roles']['private content viewer'] = 'private content viewer'; /* WAS: '' */
    $data['access comments']['roles']['site manager'] = 'site manager'; /* WAS: '' */
    $data['access comments']['roles']['special alerter'] = 'special alerter'; /* WAS: '' */
    unset($data['access comments']['roles']['anonymous user']);
    unset($data['access comments']['roles']['authenticated user']);
  }
  if (isset($data['access content'])) {
    $data['access content']['roles']['WCMS support'] = 'WCMS support'; /* WAS: '' */
    $data['access content']['roles']['content author'] = 'content author'; /* WAS: '' */
    $data['access content']['roles']['content editor'] = 'content editor'; /* WAS: '' */
    $data['access content']['roles']['emergency alerter'] = 'emergency alerter'; /* WAS: '' */
    $data['access content']['roles']['events editor'] = 'events editor'; /* WAS: '' */
    $data['access content']['roles']['form editor'] = 'form editor'; /* WAS: '' */
    $data['access content']['roles']['form results access'] = 'form results access'; /* WAS: '' */
    $data['access content']['roles']['notices editor'] = 'notices editor'; /* WAS: '' */
    $data['access content']['roles']['private content viewer'] = 'private content viewer'; /* WAS: '' */
    $data['access content']['roles']['site manager'] = 'site manager'; /* WAS: '' */
    $data['access content']['roles']['special alerter'] = 'special alerter'; /* WAS: '' */
    unset($data['access content']['roles']['anonymous user']);
    unset($data['access content']['roles']['authenticated user']);
  }
  if (isset($data['access forward'])) {
    unset($data['access forward']['roles']['administrator']);
    unset($data['access forward']['roles']['anonymous user']);
    unset($data['access forward']['roles']['authenticated user']);
  }
  if (isset($data['access news feeds'])) {
    $data['access news feeds']['roles']['WCMS support'] = 'WCMS support'; /* WAS: '' */
    $data['access news feeds']['roles']['award content author'] = 'award content author'; /* WAS: '' */
    $data['access news feeds']['roles']['content author'] = 'content author'; /* WAS: '' */
    $data['access news feeds']['roles']['content editor'] = 'content editor'; /* WAS: '' */
    $data['access news feeds']['roles']['emergency alerter'] = 'emergency alerter'; /* WAS: '' */
    $data['access news feeds']['roles']['events editor'] = 'events editor'; /* WAS: '' */
    $data['access news feeds']['roles']['form editor'] = 'form editor'; /* WAS: '' */
    $data['access news feeds']['roles']['notices editor'] = 'notices editor'; /* WAS: '' */
    $data['access news feeds']['roles']['private content viewer'] = 'private content viewer'; /* WAS: '' */
    $data['access news feeds']['roles']['site manager'] = 'site manager'; /* WAS: '' */
    $data['access news feeds']['roles']['special alerter'] = 'special alerter'; /* WAS: '' */
    unset($data['access news feeds']['roles']['anonymous user']);
    unset($data['access news feeds']['roles']['authenticated user']);
  }
  if (isset($data['access own webform submissions'])) {
    $data['access own webform submissions']['roles']['WCMS support'] = 'WCMS support'; /* WAS: '' */
    $data['access own webform submissions']['roles']['award content author'] = 'award content author'; /* WAS: '' */
    $data['access own webform submissions']['roles']['content author'] = 'content author'; /* WAS: '' */
    $data['access own webform submissions']['roles']['content editor'] = 'content editor'; /* WAS: '' */
    $data['access own webform submissions']['roles']['emergency alerter'] = 'emergency alerter'; /* WAS: '' */
    $data['access own webform submissions']['roles']['events editor'] = 'events editor'; /* WAS: '' */
    $data['access own webform submissions']['roles']['form editor'] = 'form editor'; /* WAS: '' */
    $data['access own webform submissions']['roles']['form results access'] = 'form results access'; /* WAS: '' */
    $data['access own webform submissions']['roles']['notices editor'] = 'notices editor'; /* WAS: '' */
    $data['access own webform submissions']['roles']['private content viewer'] = 'private content viewer'; /* WAS: '' */
    $data['access own webform submissions']['roles']['site manager'] = 'site manager'; /* WAS: '' */
    $data['access own webform submissions']['roles']['special alerter'] = 'special alerter'; /* WAS: '' */
    unset($data['access own webform submissions']['roles']['authenticated user']);
  }
  if (isset($data['change own e-mail'])) {
    $data['change own e-mail']['roles']['WCMS support'] = 'WCMS support'; /* WAS: '' */
    $data['change own e-mail']['roles']['award content author'] = 'award content author'; /* WAS: '' */
    $data['change own e-mail']['roles']['content author'] = 'content author'; /* WAS: '' */
    $data['change own e-mail']['roles']['content editor'] = 'content editor'; /* WAS: '' */
    $data['change own e-mail']['roles']['emergency alerter'] = 'emergency alerter'; /* WAS: '' */
    $data['change own e-mail']['roles']['events editor'] = 'events editor'; /* WAS: '' */
    $data['change own e-mail']['roles']['form editor'] = 'form editor'; /* WAS: '' */
    $data['change own e-mail']['roles']['form results access'] = 'form results access'; /* WAS: '' */
    $data['change own e-mail']['roles']['notices editor'] = 'notices editor'; /* WAS: '' */
    $data['change own e-mail']['roles']['private content viewer'] = 'private content viewer'; /* WAS: '' */
    $data['change own e-mail']['roles']['site manager'] = 'site manager'; /* WAS: '' */
    $data['change own e-mail']['roles']['special alerter'] = 'special alerter'; /* WAS: '' */
    unset($data['change own e-mail']['roles']['authenticated user']);
  }
  if (isset($data['change own openid'])) {
    $data['change own openid']['roles']['WCMS support'] = 'WCMS support'; /* WAS: '' */
    $data['change own openid']['roles']['award content author'] = 'award content author'; /* WAS: '' */
    $data['change own openid']['roles']['content author'] = 'content author'; /* WAS: '' */
    $data['change own openid']['roles']['content editor'] = 'content editor'; /* WAS: '' */
    $data['change own openid']['roles']['emergency alerter'] = 'emergency alerter'; /* WAS: '' */
    $data['change own openid']['roles']['events editor'] = 'events editor'; /* WAS: '' */
    $data['change own openid']['roles']['form editor'] = 'form editor'; /* WAS: '' */
    $data['change own openid']['roles']['form results access'] = 'form results access'; /* WAS: '' */
    $data['change own openid']['roles']['notices editor'] = 'notices editor'; /* WAS: '' */
    $data['change own openid']['roles']['private content viewer'] = 'private content viewer'; /* WAS: '' */
    $data['change own openid']['roles']['site manager'] = 'site manager'; /* WAS: '' */
    $data['change own openid']['roles']['special alerter'] = 'special alerter'; /* WAS: '' */
    unset($data['change own openid']['roles']['authenticated user']);
  }
  if (isset($data['change own password'])) {
    $data['change own password']['roles']['WCMS support'] = 'WCMS support'; /* WAS: '' */
    $data['change own password']['roles']['award content author'] = 'award content author'; /* WAS: '' */
    $data['change own password']['roles']['content author'] = 'content author'; /* WAS: '' */
    $data['change own password']['roles']['content editor'] = 'content editor'; /* WAS: '' */
    $data['change own password']['roles']['emergency alerter'] = 'emergency alerter'; /* WAS: '' */
    $data['change own password']['roles']['events editor'] = 'events editor'; /* WAS: '' */
    $data['change own password']['roles']['form editor'] = 'form editor'; /* WAS: '' */
    $data['change own password']['roles']['form results access'] = 'form results access'; /* WAS: '' */
    $data['change own password']['roles']['notices editor'] = 'notices editor'; /* WAS: '' */
    $data['change own password']['roles']['private content viewer'] = 'private content viewer'; /* WAS: '' */
    $data['change own password']['roles']['site manager'] = 'site manager'; /* WAS: '' */
    $data['change own password']['roles']['special alerter'] = 'special alerter'; /* WAS: '' */
    unset($data['change own password']['roles']['authenticated user']);
  }
  if (isset($data['edit mimemail user settings'])) {
    $data['edit mimemail user settings']['roles']['WCMS support'] = 'WCMS support'; /* WAS: '' */
    $data['edit mimemail user settings']['roles']['award content author'] = 'award content author'; /* WAS: '' */
    $data['edit mimemail user settings']['roles']['content author'] = 'content author'; /* WAS: '' */
    $data['edit mimemail user settings']['roles']['content editor'] = 'content editor'; /* WAS: '' */
    $data['edit mimemail user settings']['roles']['emergency alerter'] = 'emergency alerter'; /* WAS: '' */
    $data['edit mimemail user settings']['roles']['events editor'] = 'events editor'; /* WAS: '' */
    $data['edit mimemail user settings']['roles']['form editor'] = 'form editor'; /* WAS: '' */
    $data['edit mimemail user settings']['roles']['form results access'] = 'form results access'; /* WAS: '' */
    $data['edit mimemail user settings']['roles']['notices editor'] = 'notices editor'; /* WAS: '' */
    $data['edit mimemail user settings']['roles']['private content viewer'] = 'private content viewer'; /* WAS: '' */
    $data['edit mimemail user settings']['roles']['site manager'] = 'site manager'; /* WAS: '' */
    $data['edit mimemail user settings']['roles']['special alerter'] = 'special alerter'; /* WAS: '' */
    unset($data['edit mimemail user settings']['roles']['authenticated user']);
  }
  if (isset($data['post comments'])) {
    $data['post comments']['roles']['WCMS support'] = 'WCMS support'; /* WAS: '' */
    $data['post comments']['roles']['administrator'] = 'administrator'; /* WAS: '' */
    $data['post comments']['roles']['award content author'] = 'award content author'; /* WAS: '' */
    $data['post comments']['roles']['content author'] = 'content author'; /* WAS: '' */
    $data['post comments']['roles']['content editor'] = 'content editor'; /* WAS: '' */
    $data['post comments']['roles']['emergency alerter'] = 'emergency alerter'; /* WAS: '' */
    $data['post comments']['roles']['events editor'] = 'events editor'; /* WAS: '' */
    $data['post comments']['roles']['form editor'] = 'form editor'; /* WAS: '' */
    $data['post comments']['roles']['notices editor'] = 'notices editor'; /* WAS: '' */
    $data['post comments']['roles']['private content viewer'] = 'private content viewer'; /* WAS: '' */
    $data['post comments']['roles']['site manager'] = 'site manager'; /* WAS: '' */
    $data['post comments']['roles']['special alerter'] = 'special alerter'; /* WAS: '' */
    unset($data['post comments']['roles']['anonymous user']);
    unset($data['post comments']['roles']['authenticated user']);
  }
  if (isset($data['publish own pdfs'])) {
    $data['publish own pdfs']['roles']['WCMS support'] = 'WCMS support'; /* WAS: '' */
    $data['publish own pdfs']['roles']['award content author'] = 'award content author'; /* WAS: '' */
    $data['publish own pdfs']['roles']['content author'] = 'content author'; /* WAS: '' */
    $data['publish own pdfs']['roles']['content editor'] = 'content editor'; /* WAS: '' */
    $data['publish own pdfs']['roles']['emergency alerter'] = 'emergency alerter'; /* WAS: '' */
    $data['publish own pdfs']['roles']['events editor'] = 'events editor'; /* WAS: '' */
    $data['publish own pdfs']['roles']['form results access'] = 'form results access'; /* WAS: '' */
    $data['publish own pdfs']['roles']['notices editor'] = 'notices editor'; /* WAS: '' */
    $data['publish own pdfs']['roles']['private content viewer'] = 'private content viewer'; /* WAS: '' */
    $data['publish own pdfs']['roles']['site manager'] = 'site manager'; /* WAS: '' */
    $data['publish own pdfs']['roles']['special alerter'] = 'special alerter'; /* WAS: '' */
    unset($data['publish own pdfs']['roles']['anonymous user']);
    unset($data['publish own pdfs']['roles']['authenticated user']);
  }
  if (isset($data['search uw_project content'])) {
    $data['search uw_project content']['roles']['WCMS support'] = 'WCMS support'; /* WAS: '' */
    $data['search uw_project content']['roles']['award content author'] = 'award content author'; /* WAS: '' */
    $data['search uw_project content']['roles']['content author'] = 'content author'; /* WAS: '' */
    $data['search uw_project content']['roles']['content editor'] = 'content editor'; /* WAS: '' */
    $data['search uw_project content']['roles']['emergency alerter'] = 'emergency alerter'; /* WAS: '' */
    $data['search uw_project content']['roles']['events editor'] = 'events editor'; /* WAS: '' */
    $data['search uw_project content']['roles']['form editor'] = 'form editor'; /* WAS: '' */
    $data['search uw_project content']['roles']['notices editor'] = 'notices editor'; /* WAS: '' */
    $data['search uw_project content']['roles']['private content viewer'] = 'private content viewer'; /* WAS: '' */
    $data['search uw_project content']['roles']['site manager'] = 'site manager'; /* WAS: '' */
    $data['search uw_project content']['roles']['special alerter'] = 'special alerter'; /* WAS: '' */
    unset($data['search uw_project content']['roles']['anonymous user']);
    unset($data['search uw_project content']['roles']['authenticated user']);
  }
  if (isset($data['search uw_service content'])) {
    $data['search uw_service content']['roles']['WCMS support'] = 'WCMS support'; /* WAS: '' */
    $data['search uw_service content']['roles']['award content author'] = 'award content author'; /* WAS: '' */
    $data['search uw_service content']['roles']['content author'] = 'content author'; /* WAS: '' */
    $data['search uw_service content']['roles']['content editor'] = 'content editor'; /* WAS: '' */
    $data['search uw_service content']['roles']['emergency alerter'] = 'emergency alerter'; /* WAS: '' */
    $data['search uw_service content']['roles']['events editor'] = 'events editor'; /* WAS: '' */
    $data['search uw_service content']['roles']['form editor'] = 'form editor'; /* WAS: '' */
    $data['search uw_service content']['roles']['notices editor'] = 'notices editor'; /* WAS: '' */
    $data['search uw_service content']['roles']['private content viewer'] = 'private content viewer'; /* WAS: '' */
    $data['search uw_service content']['roles']['site manager'] = 'site manager'; /* WAS: '' */
    $data['search uw_service content']['roles']['special alerter'] = 'special alerter'; /* WAS: '' */
    unset($data['search uw_service content']['roles']['anonymous user']);
    unset($data['search uw_service content']['roles']['authenticated user']);
  }
}
